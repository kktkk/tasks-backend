import os
import boto3
import uuid
from flask import Flask, jsonify, request
from boto3.dynamodb.conditions import Key
from flask_cors import CORS


TASKS_TABLE = os.environ['TASKS_TABLE']
client = boto3.client('dynamodb')
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(TASKS_TABLE)

app = Flask(__name__)
CORS(app)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/tasks")
def get_tasks():
    resp = table.scan()
    items = resp['Items']
    if not items:
        return jsonify({'error': 'Could not fetch tasks'}), 404

    return jsonify(items)


@app.route("/tasks/<string:task_id>")
def get_task(task_id):
    resp = client.get_item(
        TableName=TASKS_TABLE,
        Key={
            'taskId': { 'S': task_id }
        }
    )
    item = resp.get('Item')
    if not item:
        return jsonify({'error': 'Task does not exist'}), 404

    return jsonify(item)


@app.route("/tasks/new", methods=["POST"])
def create_task():
    task_id = str(uuid.uuid4()).replace("-", "")
    name = request.json.get('name')
    text = request.json.get('text')
    if not name or not text:
        return jsonify({'error': 'Please provide name and text'}), 400

    resp = client.put_item(
        TableName=TASKS_TABLE,
        Item={
            'taskId': {'S': task_id },
            'name': {'S': name },
            'text': {'S': text}
        }
    )

    return jsonify({
        'taskId': task_id
    })
